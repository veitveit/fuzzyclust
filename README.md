# FuzzyClust Shiny App #

The app applies fuzzy c-means clustering to multi-variate data. The parameters of the algorithm are calculated automatically or can be chosen from validation indices.
You can test the app at [computproteomics.bmb.sdu.dk](http://computproteomics.bmb.sdu.dk)

### Literature ###
Fuzzy c-means clustering Parameters are estimated according to Schwämmle, V. and Jensen, O. N. A simple and fast method to determine the parameters for fuzzy c-means cluster analysis. Bioinformatics, 2010, 26, 2841-2848. [PubMed](http://www.ncbi.nlm.nih.gov/pubmed/20880957)

### Data input ###
Input file is csv containing only the to-be-clustered values and optionally row and column names. 
Output file provides membership values. Be aware that cluster members with a maximum membership value below 
0.5 are not shown in the figure and should no be considered.

### How do I run the program? ###
The easiest way is to install [RStudio](https://www.rstudio.com/) on your computer and run it from there. Alternatively, you can run the files on a [shiny server](https://www.rstudio.com/products/shiny/shiny-server2/) environment.

### Issues ###
For bug reports and general problems, please submit an issue.